﻿
namespace Casita_Robada
{
    partial class frmInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInicio));
            this.lblMano2 = new System.Windows.Forms.Label();
            this.lblMano1 = new System.Windows.Forms.Label();
            this.picMazo = new System.Windows.Forms.PictureBox();
            this.picCartaTablero10 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero5 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero6 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero1 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero9 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero4 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero3 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero2 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero7 = new System.Windows.Forms.PictureBox();
            this.picCartaTablero8 = new System.Windows.Forms.PictureBox();
            this.picCasitaPj = new System.Windows.Forms.PictureBox();
            this.picCasitaCPU = new System.Windows.Forms.PictureBox();
            this.picManoJugador3 = new System.Windows.Forms.PictureBox();
            this.picManoJugador2 = new System.Windows.Forms.PictureBox();
            this.picManoJugador1 = new System.Windows.Forms.PictureBox();
            this.picManoCPU3 = new System.Windows.Forms.PictureBox();
            this.picManoCPU2 = new System.Windows.Forms.PictureBox();
            this.picManoCPU1 = new System.Windows.Forms.PictureBox();
            this.imgListMazo = new System.Windows.Forms.ImageList(this.components);
            this.btnRepartir = new System.Windows.Forms.Button();
            this.btnTirar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picMazo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCasitaPj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCasitaCPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoJugador3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoJugador2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoJugador1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoCPU3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoCPU2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoCPU1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMano2
            // 
            this.lblMano2.AutoSize = true;
            this.lblMano2.Font = new System.Drawing.Font("Goudy Old Style", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMano2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblMano2.Location = new System.Drawing.Point(272, 489);
            this.lblMano2.Name = "lblMano2";
            this.lblMano2.Size = new System.Drawing.Size(102, 23);
            this.lblMano2.TabIndex = 0;
            this.lblMano2.Text = "Mano del Pj\r\n";
            // 
            // lblMano1
            // 
            this.lblMano1.AutoSize = true;
            this.lblMano1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblMano1.Font = new System.Drawing.Font("Goudy Old Style", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMano1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblMano1.Location = new System.Drawing.Point(253, 9);
            this.lblMano1.Name = "lblMano1";
            this.lblMano1.Size = new System.Drawing.Size(138, 23);
            this.lblMano1.TabIndex = 1;
            this.lblMano1.Text = "Mano de la CPU";
            this.lblMano1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picMazo
            // 
            this.picMazo.BackColor = System.Drawing.Color.Black;
            this.picMazo.Image = global::Casita_Robada.Properties.Resources.Carta2;
            this.picMazo.Location = new System.Drawing.Point(12, 391);
            this.picMazo.Name = "picMazo";
            this.picMazo.Size = new System.Drawing.Size(82, 118);
            this.picMazo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picMazo.TabIndex = 20;
            this.picMazo.TabStop = false;
            // 
            // picCartaTablero10
            // 
            this.picCartaTablero10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero10.Location = new System.Drawing.Point(509, 272);
            this.picCartaTablero10.Name = "picCartaTablero10";
            this.picCartaTablero10.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero10.TabIndex = 19;
            this.picCartaTablero10.TabStop = false;
            // 
            // picCartaTablero5
            // 
            this.picCartaTablero5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero5.Location = new System.Drawing.Point(509, 159);
            this.picCartaTablero5.Name = "picCartaTablero5";
            this.picCartaTablero5.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero5.TabIndex = 18;
            this.picCartaTablero5.TabStop = false;
            // 
            // picCartaTablero6
            // 
            this.picCartaTablero6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero6.Location = new System.Drawing.Point(73, 272);
            this.picCartaTablero6.Name = "picCartaTablero6";
            this.picCartaTablero6.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero6.TabIndex = 17;
            this.picCartaTablero6.TabStop = false;
            // 
            // picCartaTablero1
            // 
            this.picCartaTablero1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero1.Location = new System.Drawing.Point(73, 159);
            this.picCartaTablero1.Name = "picCartaTablero1";
            this.picCartaTablero1.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero1.TabIndex = 16;
            this.picCartaTablero1.TabStop = false;
            // 
            // picCartaTablero9
            // 
            this.picCartaTablero9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero9.Location = new System.Drawing.Point(396, 272);
            this.picCartaTablero9.Name = "picCartaTablero9";
            this.picCartaTablero9.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero9.TabIndex = 15;
            this.picCartaTablero9.TabStop = false;
            // 
            // picCartaTablero4
            // 
            this.picCartaTablero4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero4.Location = new System.Drawing.Point(396, 159);
            this.picCartaTablero4.Name = "picCartaTablero4";
            this.picCartaTablero4.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero4.TabIndex = 14;
            this.picCartaTablero4.TabStop = false;
            // 
            // picCartaTablero3
            // 
            this.picCartaTablero3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero3.Location = new System.Drawing.Point(290, 159);
            this.picCartaTablero3.Name = "picCartaTablero3";
            this.picCartaTablero3.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero3.TabIndex = 13;
            this.picCartaTablero3.TabStop = false;
            // 
            // picCartaTablero2
            // 
            this.picCartaTablero2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero2.Location = new System.Drawing.Point(180, 159);
            this.picCartaTablero2.Name = "picCartaTablero2";
            this.picCartaTablero2.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero2.TabIndex = 12;
            this.picCartaTablero2.TabStop = false;
            // 
            // picCartaTablero7
            // 
            this.picCartaTablero7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero7.Location = new System.Drawing.Point(180, 272);
            this.picCartaTablero7.Name = "picCartaTablero7";
            this.picCartaTablero7.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero7.TabIndex = 11;
            this.picCartaTablero7.TabStop = false;
            this.picCartaTablero7.Click += new System.EventHandler(this.pictureBox10_Click);
            // 
            // picCartaTablero8
            // 
            this.picCartaTablero8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.picCartaTablero8.Location = new System.Drawing.Point(290, 272);
            this.picCartaTablero8.Name = "picCartaTablero8";
            this.picCartaTablero8.Size = new System.Drawing.Size(67, 85);
            this.picCartaTablero8.TabIndex = 10;
            this.picCartaTablero8.TabStop = false;
            this.picCartaTablero8.Click += new System.EventHandler(this.pictureBox9_Click);
            // 
            // picCasitaPj
            // 
            this.picCasitaPj.BackColor = System.Drawing.Color.Yellow;
            this.picCasitaPj.Location = new System.Drawing.Point(557, 390);
            this.picCasitaPj.Name = "picCasitaPj";
            this.picCasitaPj.Size = new System.Drawing.Size(82, 118);
            this.picCasitaPj.TabIndex = 9;
            this.picCasitaPj.TabStop = false;
            // 
            // picCasitaCPU
            // 
            this.picCasitaCPU.BackColor = System.Drawing.Color.Yellow;
            this.picCasitaCPU.Location = new System.Drawing.Point(12, 12);
            this.picCasitaCPU.Name = "picCasitaCPU";
            this.picCasitaCPU.Size = new System.Drawing.Size(82, 118);
            this.picCasitaCPU.TabIndex = 8;
            this.picCasitaCPU.TabStop = false;
            // 
            // picManoJugador3
            // 
            this.picManoJugador3.Location = new System.Drawing.Point(396, 390);
            this.picManoJugador3.Name = "picManoJugador3";
            this.picManoJugador3.Size = new System.Drawing.Size(67, 85);
            this.picManoJugador3.TabIndex = 7;
            this.picManoJugador3.TabStop = false;
            this.picManoJugador3.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // picManoJugador2
            // 
            this.picManoJugador2.Location = new System.Drawing.Point(290, 390);
            this.picManoJugador2.Name = "picManoJugador2";
            this.picManoJugador2.Size = new System.Drawing.Size(67, 85);
            this.picManoJugador2.TabIndex = 6;
            this.picManoJugador2.TabStop = false;
            this.picManoJugador2.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // picManoJugador1
            // 
            this.picManoJugador1.Location = new System.Drawing.Point(180, 390);
            this.picManoJugador1.Name = "picManoJugador1";
            this.picManoJugador1.Size = new System.Drawing.Size(67, 85);
            this.picManoJugador1.TabIndex = 5;
            this.picManoJugador1.TabStop = false;
            this.picManoJugador1.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // picManoCPU3
            // 
            this.picManoCPU3.Location = new System.Drawing.Point(396, 45);
            this.picManoCPU3.Name = "picManoCPU3";
            this.picManoCPU3.Size = new System.Drawing.Size(67, 85);
            this.picManoCPU3.TabIndex = 4;
            this.picManoCPU3.TabStop = false;
            // 
            // picManoCPU2
            // 
            this.picManoCPU2.Location = new System.Drawing.Point(290, 45);
            this.picManoCPU2.Name = "picManoCPU2";
            this.picManoCPU2.Size = new System.Drawing.Size(67, 85);
            this.picManoCPU2.TabIndex = 3;
            this.picManoCPU2.TabStop = false;
            // 
            // picManoCPU1
            // 
            this.picManoCPU1.Location = new System.Drawing.Point(180, 45);
            this.picManoCPU1.Name = "picManoCPU1";
            this.picManoCPU1.Size = new System.Drawing.Size(67, 85);
            this.picManoCPU1.TabIndex = 2;
            this.picManoCPU1.TabStop = false;
            // 
            // imgListMazo
            // 
            this.imgListMazo.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListMazo.ImageStream")));
            this.imgListMazo.TransparentColor = System.Drawing.Color.Transparent;
            this.imgListMazo.Images.SetKeyName(0, "1 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(1, "2 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(2, "3 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(3, "4 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(4, "5 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(5, "6 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(6, "7 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(7, "8 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(8, "9 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(9, "10 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(10, "11 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(11, "12 de copa.PNG");
            this.imgListMazo.Images.SetKeyName(12, "1 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(13, "2 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(14, "3 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(15, "4 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(16, "5 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(17, "6 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(18, "7 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(19, "8 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(20, "9 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(21, "10 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(22, "11 de espada.PNG");
            this.imgListMazo.Images.SetKeyName(23, "12 de esapada.PNG");
            this.imgListMazo.Images.SetKeyName(24, "1 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(25, "2 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(26, "3 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(27, "4 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(28, "5 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(29, "5 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(30, "6 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(31, "7 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(32, "8 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(33, "9 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(34, "10 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(35, "11 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(36, "12 de basto.PNG");
            this.imgListMazo.Images.SetKeyName(37, "1 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(38, "2 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(39, "3 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(40, "4 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(41, "5 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(42, "6 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(43, "7 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(44, "8 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(45, "9 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(46, "10 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(47, "11 de oro.PNG");
            this.imgListMazo.Images.SetKeyName(48, "12 de oro.PNG");
            // 
            // btnRepartir
            // 
            this.btnRepartir.BackColor = System.Drawing.Color.Transparent;
            this.btnRepartir.ForeColor = System.Drawing.Color.Black;
            this.btnRepartir.Location = new System.Drawing.Point(548, 36);
            this.btnRepartir.Name = "btnRepartir";
            this.btnRepartir.Size = new System.Drawing.Size(75, 41);
            this.btnRepartir.TabIndex = 21;
            this.btnRepartir.Text = "Repartir";
            this.btnRepartir.UseVisualStyleBackColor = false;
            this.btnRepartir.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnTirar
            // 
            this.btnTirar.Location = new System.Drawing.Point(651, 220);
            this.btnTirar.Name = "btnTirar";
            this.btnTirar.Size = new System.Drawing.Size(84, 40);
            this.btnTirar.TabIndex = 22;
            this.btnTirar.Text = "Tirar";
            this.btnTirar.UseVisualStyleBackColor = true;
            this.btnTirar.Click += new System.EventHandler(this.btnTirar_Click);
            // 
            // frmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(766, 521);
            this.Controls.Add(this.btnTirar);
            this.Controls.Add(this.btnRepartir);
            this.Controls.Add(this.picMazo);
            this.Controls.Add(this.picCartaTablero10);
            this.Controls.Add(this.picCartaTablero5);
            this.Controls.Add(this.picCartaTablero6);
            this.Controls.Add(this.picCartaTablero1);
            this.Controls.Add(this.picCartaTablero9);
            this.Controls.Add(this.picCartaTablero4);
            this.Controls.Add(this.picCartaTablero3);
            this.Controls.Add(this.picCartaTablero2);
            this.Controls.Add(this.picCartaTablero7);
            this.Controls.Add(this.picCartaTablero8);
            this.Controls.Add(this.picCasitaPj);
            this.Controls.Add(this.picCasitaCPU);
            this.Controls.Add(this.picManoJugador3);
            this.Controls.Add(this.picManoJugador2);
            this.Controls.Add(this.picManoJugador1);
            this.Controls.Add(this.picManoCPU3);
            this.Controls.Add(this.picManoCPU2);
            this.Controls.Add(this.picManoCPU1);
            this.Controls.Add(this.lblMano1);
            this.Controls.Add(this.lblMano2);
            this.Name = "frmInicio";
            this.Text = "Casita Robada";
            this.Load += new System.EventHandler(this.Inicio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picMazo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCartaTablero8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCasitaPj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCasitaCPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoJugador3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoJugador2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoJugador1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoCPU3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoCPU2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManoCPU1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMano2;
        private System.Windows.Forms.Label lblMano1;
        private System.Windows.Forms.PictureBox picManoCPU1;
        private System.Windows.Forms.PictureBox picManoCPU2;
        private System.Windows.Forms.PictureBox picManoCPU3;
        private System.Windows.Forms.PictureBox picManoJugador1;
        private System.Windows.Forms.PictureBox picManoJugador2;
        private System.Windows.Forms.PictureBox picManoJugador3;
        private System.Windows.Forms.PictureBox picCasitaCPU;
        private System.Windows.Forms.PictureBox picCasitaPj;
        private System.Windows.Forms.PictureBox picCartaTablero8;
        private System.Windows.Forms.PictureBox picCartaTablero7;
        private System.Windows.Forms.PictureBox picCartaTablero2;
        private System.Windows.Forms.PictureBox picCartaTablero3;
        private System.Windows.Forms.PictureBox picCartaTablero4;
        private System.Windows.Forms.PictureBox picCartaTablero9;
        private System.Windows.Forms.PictureBox picCartaTablero1;
        private System.Windows.Forms.PictureBox picCartaTablero6;
        private System.Windows.Forms.PictureBox picCartaTablero5;
        private System.Windows.Forms.PictureBox picCartaTablero10;
        private System.Windows.Forms.PictureBox picMazo;
        private System.Windows.Forms.ImageList imgListMazo;
        private System.Windows.Forms.Button btnRepartir;
        private System.Windows.Forms.Button btnTirar;
    }
}