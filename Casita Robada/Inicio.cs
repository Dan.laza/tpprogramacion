﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP_cartas;

namespace Casita_Robada
{
    public partial class frmInicio : Form
    {

        public frmInicio()
        {
            InitializeComponent();
        }

        private void Inicio_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            TP_cartas.ManagerJuego manager = new TP_cartas.ManagerJuego();
            Mazo mazo = manager.mazoCartas;
            Pj pj = new Pj();
            CPU cpu = new CPU();

            bool turno = false;
            for(int repartir=0;repartir<6;repartir++)
            {
                if(turno)
                {
                    cpu.cartas.Add(mazo.Pedir());
                }
                else
                {

                    pj.cartas.Add(mazo.Pedir());
                }
                turno = !turno;
            }
            this.picManoJugador1.Image = imgListMazo.Images[pj.cartas[0].pos_imagen];
            this.picManoJugador2.Image = imgListMazo.Images[pj.cartas[1].pos_imagen];
            this.picManoJugador3.Image = imgListMazo.Images[pj.cartas[2].pos_imagen];
            this.picManoCPU1.Image = imgListMazo.Images[cpu.cartas[0].pos_imagen];
            this.picManoCPU2.Image = imgListMazo.Images[cpu.cartas[1].pos_imagen];
            this.picManoCPU3.Image = imgListMazo.Images[cpu.cartas[2].pos_imagen];
        }

        private void btnTirar_Click(object sender, EventArgs e)
        {
            
        }
    }
}
